Project and Programming Standards
=================================

## Abstract
The goal of the documents within this repository is to manage the
complexity of the various languages utilized by programmers today.

These documents will outline the fundamental and defacto standards
of many different langauges by detailing the *do*s and *don't*s of
programming with them. The rules that will be presented exist to
negate programming errors and keep the code base manageable while
still allowing programmers to use their languages productively.

One way in which we keep the code base manageable is by enforcing
*consistency*. It is extremely important that any programmer be
able to look at another's code and quickly understand it. By
demanding consistent code, we develop means for programmers to
logically apply pattern matching to segments of unknown code to
better understand it and infer its purpose. 

_In closing..._
Creating common idioms and patterns makes code easier to understand.